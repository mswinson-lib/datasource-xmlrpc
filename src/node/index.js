const getXmlRpc = require('./utils/get_xmlrpc.js');

// lambda handler
// @param event [Object] payload object
// @param context [Object] context object
// @param callback [Function] callback function
// @return None
exports.handler = async function(event, context, callback) {
  var url = event.url;
  var method = event.method;
  var args = event.args;

  if (url !== null) {
    var json = await getXmlRpc(url, method, args);

    callback(null, json);
    return;
  } else {
    callback(null, {});
    return;
  }
}
