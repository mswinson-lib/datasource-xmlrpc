const xmlrpc = require('xmlrpc');

// create client for url
// @param url [String] http or https url
// @return [Client]
// @raise InvalidProtocol - if protocol not http or https
var client_for = function(url) {
  if (url.match(/^https:/)) {
    return xmlrpc.createSecureClient(url);
  } 

  if (url.match(/^http:/)) {
    return xmlrpc.createClient(url);
  }

  throw "Invalid protocol. Use http or https";
}

// get xmlrpc results
// @param url [String] xmlrpc endpoint
// @param method [String] method name
// @param args [null|Array] method arguments
// @return [Promise]
var getXmlRpc = function(url, method, args = null) {
	return new Promise((resolve,reject) => {
    var client = client_for(url);
    client.methodCall(method, args, function(err, result) {
      resolve(result);
    });
  });
}

module.exports = exports = getXmlRpc;
