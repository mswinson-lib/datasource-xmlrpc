.PHONY: help server test build deploy

STACK_NAME=datasource-xmlrpc

S3_BUCKET_NAME ?= repo.dev.mswinson.com
S3_BUCKET_PREFIX ?= packages/com.mswinson/lib/cf/$(STACK_NAME)
S3_OPTIONS=--s3-bucket $(S3_BUCKET_NAME) --s3-prefix $(S3_BUCKET_PREFIX)

AWS_DEFAULT_REGION ?= us-east-1

BUILD_ENV ?= "sandbox"

ifeq ($(BUILD_ENV), "sandbox")
	NPM := docker run -it --rm \
		-v ${ROOTDIR}/src/node:/var/app \
		--workdir /var/app \
		--entrypoint npm \
		node:8.10-alpine 
else
	NPM := docker run --rm \
		-v "${ROOTDIR}/src/node":/var/app \
		--workdir /var/app \
		--entrypoint npm \
		node:8.10-alpine
endif

help:
	@sam

test:
	sam validate

build:
	@$(NPM) install
	@mkdir -p ./target
	@sam package --template-file ./template.yml --use-json --output-template-file target/$(STACK_NAME).json $(S3_OPTIONS)

deploy:
	@AWS_DEFAULT_REGION=$(AWS_DEFAULT_REGION) sam deploy --template-file ./target/$(STACK_NAME).json --stack-name $(STACK_NAME) $(S3_OPTIONS) --capabilities CAPABILITY_IAM
